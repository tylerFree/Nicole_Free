In this coding exercise I have created a nav and header with some content on the page. The page 
can be broken up into 3 sections.

Header/Nav

To Do: 

    1.On mobile , there is a hamburger menu that on click will animate to show a clicked state. The Nav menu is hiding off
    the page. Use JS to slide the Nav menu in and out when the hamburger menu is toggled.


    2.On Desktop and Mobile. When a nav menu item is clicked, have the page scroll to the proper section marked 
    in the HTML markup. On mobile , the nav menu will have to have its active state removed.

    3. On Desktop add hovers onto nav menu items.


Content Layout

    1. Lay the content out however you please. Add styles, and any additional HTML elements you may need.
    2. Add button hovers and styles.


Slider 

    1. Make the slider work using JS. Currently there are some base styles to make the slider images stack on top
    of eachother and act like a slider. Feel free to change any current styles and add new ones. Try to stick with
    the structure of the current markup. You can either make the slider loop infinite or it can stop at the end and the next button will become disabled.


Modal 

    1. The modal also has some base styles to make it work and function as a modal. There will be a link tag that when you
    click it the modal will appear. Feel free to add any styles or markup you may need.



    If you have any questions please let me know :) I am just using css , if you would like to write scss and use 
    a compiler that is cool as well! Feel free to write Jquery or ES6/ES5 whatever you like.

    After you are finished push up file to gitlab under a new branch.
